# Programa SAXPY

Este programa es una formula matemática donde dados un vector vx y vy y un escalar a se hace la siguiente fórmula

`y = x*a + y`
__________________________________

## Compilación
 Se compila el código fuente con el comando siguiente `gcc -fopenmp -o SAXPY SAXPY.c`.


 Se jecuta el archivo SAXPY por medio del comando `./SAXPY`.

------------------------

## Resultados

Se utilizaron los 3 tamaños diferentes de vector y se les aplicó la función el SAXPY serialmente y paralelamente.

Primero con un vector de 10 y se muestra que para ejecuciones no muy grandes no hay ventaja al paralelizar ya que dura más con openmp que sin fopenmp.

El segundo vector, se le dio un tamaño de 50 y nuevamente la paralelización no fue tan efectiva, esta vez si más cercano el tiempo serial.

Por ultimo se aumentó el tamaño mucho más y se le asignó un tamaño de 150, con este tamaño si mejoró el tiempo del SAXPY serial.

Se agrega la imagen de los Resultados

![alt text](resultados.png)
