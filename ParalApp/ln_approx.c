/**
*
* Este programa hace la aproximación de logaritmo naturar de 2.
* Usando la serie de ln(1+x) con -1 < x <= 1
*
**/

#include <stdio.h>
#include <omp.h>

#define X -0.189
#define STEPS 10000

double pow_s(double x, int n);

int main(int argc, char const *argv[]) {
  double ln_res = 0;
  double start_time = omp_get_wtime();
  #pragma omp parallel for
  for(int i = 1; i <= STEPS; i++){
    if((i % 2) == 1){
      double iteration = (pow_s((double)X, i)/(double)i);
      ln_res = ln_res + iteration;
    } else {
      double iteration = (pow_s((double)X, i)/(double)i);
      ln_res = ln_res - iteration;
    }
  }
  double run_time = omp_get_wtime() - start_time;

  printf("Resultado de la aproximación %f tiempo de ejecucion %f\n", ln_res, run_time );

  return 0;
}

double pow_s(double x, int n){
  double res = 1;
  //#pragma omp parallel for  reduction(*:res)
  for(int i = 0; i < n; i++){
    res *= x;
  }
  return res;
}
