# Programa de aproximcion de Logaritmo Natural

Este programa es utiliza la aproximación del logartimo natural por medio de un serie, tomando solo en cuenta los siguientes limites

`log(1 + x) 1 < x <= 1`
__________________________________

## Compilación
 Se compila el código fuente con el comando siguiente `gcc -fopenmp -o ln_approx ln_approx.c`.


 Se jecuta el archivo ln_approx por medio del comando `./ln_approx`.

------------------------

## Resultados

Se calcula la aproximación de logaritmo por medio de sumas, utilizando 5000 pasos para que la paraleliación tenga efecto. Se colocó el pragma en dos partes del código fuente, en pruebas separadas para medir el impacto en el tiempo de ejecución. Se coloco en el for que calcula la serie y en el for que calcaula la potencia. Ambas posiciones del pragma lograron mejorar que sin paralelización, pero la que más tuvo efeco fue el pragma que estaba sobre el for que calcula la serie.

En la siguiente imagen resultados del calculo.

![alt text](SerieLogaritmoN.png)
